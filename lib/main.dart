import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  void playNote({int noteNr}) => AudioCache().play('note$noteNr.wav');

  Expanded singleKey({int keyNumber, Color colorName}) => Expanded(
        child: FlatButton(
          color: colorName,
          onPressed: () {
            playNote(noteNr: keyNumber);
          },
          child: null,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              singleKey(keyNumber: 1, colorName: Colors.red),
              singleKey(keyNumber: 2, colorName: Colors.orange),
              singleKey(keyNumber: 3, colorName: Colors.yellow),
              singleKey(keyNumber: 4, colorName: Colors.green),
              singleKey(keyNumber: 5, colorName: Colors.teal),
              singleKey(keyNumber: 6, colorName: Colors.blue),
              singleKey(keyNumber: 7, colorName: Colors.indigo),
            ],
          ),
        ),
      ),
    );
  }
}
